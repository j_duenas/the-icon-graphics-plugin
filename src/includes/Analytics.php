<?php

namespace TIGP;


class Analytics
{
    protected $analytics_id;

    public function __construct($analytics_id)
    {
        $this->analytics_id = $analytics_id;
    }

    public function getHTML()
    {
        $html = sprintf("<script async src='https://www.googletagmanager.com/gtag/js?id=%s'></script>
        <script>
            window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());
        
          gtag('config', '%s');
        </script>\n", $this->analytics_id, $this->analytics_id);

        return $html;
    }
}