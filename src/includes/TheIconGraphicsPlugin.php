<?php

namespace TIGP;

class TheIconGraphicsPlugin
{

    public function __construct()
    {
        if(!TIGP_DEBUG){
            $this->productionMode();
        }

        $this->initialize();
    }

    protected function initialize()
    {
        add_filter('acf/settings/path', [$this, 'getACFPluginPath']);
        add_filter('acf/settings/dir', [$this, 'getACFPluginDir']);
        $this->createACFOptionsPage();
        add_action('wp_head', [$this, 'setFavicons'], 1);
        add_action('wp_footer', [$this, 'addAnalytics'], 1);

        if($this->getSetting('include_old_browser_notifications')){
            add_action('wp_enqueue_scripts', [$this, 'addOldBrowserDetection'], 1);
        }
    }

    public function addOldBrowserDetection()
    {
        wp_enqueue_script('old_browser_detection', plugin_dir_url(__FILE__) . '../assets/js/old-browser-warning.js');
    }

    public function addAnalytics()
    {
        $analytics = new Analytics(
            $this->getSetting('google_analytics_id')
        );

        echo $analytics->getHTML();
    }

    public function setFavicons()
    {
        $shortcut_icon = new Favicon(
            $this->getSetting('favicon'),
            'favicon'
        );
        $ios_icon = new Favicon(
            $this->getSetting('ios_icon'),
            'ios'
        );

        echo $shortcut_icon->getHTML();
        echo $ios_icon->getHTML();
    }

    public function getACFPluginPath($path)
    {
        return plugin_dir_path(__FILE__) . '../vendor/advanced-custom-fields-pro/';
    }

    public function getACFPluginDir($dir)
    {
        return plugin_dir_url(__FILE__) . '../vendor/advanced-custom-fields-pro/';
    }

    protected function createACFOptionsPage()
    {
        acf_add_options_page([
            'page_title'    =>  'The Icon Graphics Plugin Settings',
            'menu_title'    =>  'TICG Settings',
            'menu_slug'     =>  'ticg-settings',
            'parent_slug'   =>  'options-general.php',
            'capability'    =>  'edit_posts',
        ]);
    }

    public function getSetting($setting_name)
    {
        return get_field($setting_name, 'options');
    }

    protected function productionMode()
    {
        // Hides ACF dashboard menu entry
        add_filter('acf/settings/show_admin', '__return_false');
    }
}