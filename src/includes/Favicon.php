<?php
/**
 * Created by PhpStorm.
 * User: jeremy
 * Date: 1/23/2018
 * Time: 3:23 PM
 */

namespace TIGP;


class Favicon
{
    public  $icon_url,
            $icon_type = 'favicon';

    public function __construct($icon_url, $icon_type)
    {
        $this->favicon_url = $icon_url;
        $this->icon_type = $icon_type ? $icon_type : $this->icon_type;
    }

    public function getHTML()
    {
        if($this->icon_type === "favicon") {
            $html = sprintf("<link rel='shortcut icon' href='%s'>\n", $this->favicon_url);
        } else{
            $html = sprintf("<link ref='apple-touch-icon-precomposed' sizes='144x144' href='%s'>\n", $this->favicon_url);
        }
        return $html;
    }
}