<?php
/*
 * Plugin Name: The Icon Graphics Plugin
 * Version: 3.0.0
 * Author: Icon Graphics (Jeremy Duenas)
 */
namespace TIGP;

define('TIGP_DEBUG', true); //Toggles ACF admin menu, for one.

require plugin_dir_path(__FILE__) . 'vendor/advanced-custom-fields-pro/acf.php';
require plugin_dir_path(__FILE__) . 'includes/TheIconGraphicsPlugin.php';
require plugin_dir_path(__FILE__) . 'includes/Favicon.php';
require plugin_dir_path(__FILE__) . 'includes/Analytics.php';

$tigp = new TheIconGraphicsPlugin();